# README #

Version : 1.0.0

* Coronavirus Analysis Based on Kaggle Dataset( Country Specific )

The World Health Organization recognized SARS-CoV-2 as a public health concern and declared it as a pandemic on March 11, 2020. 
Over 12 million people have been affected across several countries since it was first recognized.
SARS-CoV-2 is thought to commonly spread via respiratory droplets formed while talking, coughing, and sneezing of an infected patient.

Coronavirus Tracker :  https://www.bing.com/covid?form=COVD07

Coronavirus Risk Factors From Experts : 23-MAR-2021

* Autoimmune disorders
* Age { Immunity }
* Asthma
* Cancer
* Cardio & Cerebrovascular disease
* Chronic kidney disease
* Chronic liver disease
* Chronic respiratory diseases
* COPD
* Dementia
* Diabetes
* Endocrine diseases
* Ethnicity_ Hispanic
* Heart Disease
* Heart Failure
* Hypertension
* Immune system disorders
* Overweight or obese
* Respiratory system diseases
* Smoking Status
